
const { ccclass, property } = cc._decorator;

@ccclass
export default class JsZipScene extends cc.Component {

    m_cfgTableMap: any = {};

    start() {

        this.m_cfgTableMap = {};

        this.loadZip();
    }

    loadZip() {
        let self = this;
        let url = cc.url.raw('resources/configs.zip');
        cc.loader.load({ url: url, type: "binary" }, (err: Error, zipData: ArrayBuffer) => {
            //2.1.3在安卓平台下会出现load不到资源的情况
            if (err) {
                let httpUrl = `资源服务地址${url}`;
                console.log('loadConfigZip httpUrl: ', httpUrl);
                let oReq = new XMLHttpRequest();
                oReq.open("GET", httpUrl, true);
                oReq.responseType = "arraybuffer";

                oReq.onload = function (oEvent) {
                    let arrayBuffer = oReq.response; // 注意:不是oReq.responseText
                    if (arrayBuffer) {
                        console.log('LoadConfig::unzip 0');
                        self.unzip(arrayBuffer);
                    }
                };
                oReq.send(null);
            }
            else {
                console.log("unzip unzip");
                this.unzip(zipData);
            }
        });
    }

    unzip(zipData) {
        let self = this;
        let newZip = new JSZip(); // 因为将jszip导入为插件，所以可以全局直接访问
        newZip.loadAsync(zipData).then(zip => {
            // console.log(zip);
            let fileList = zip.files;
            for (let filename in fileList) {
                zip.file(filename).async('string').then(data => {
                    let json = JSON.parse(data);
                    console.log(JSON.stringify(json));
                    self.m_cfgTableMap[filename] = json;
                });
            }

        });
    }

}
